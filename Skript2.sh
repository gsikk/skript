#!/bin/bash
#Gert Sikk (A22)
#Veebihosting/Veebiteenus - BASH kodutöö nr 2.
#Skript loob uue veebi kodu



#Kontrollib, kas skript on käivitatud juurkasutajana, kui pole juur, siis ei saa installida.
if [  $UID -ne 0 ]
then
   echo "käivita skript $(basename $0) juurkasutaja õigustes"
   exit 1   
fi

#Kontrollib, kas on etteantud õige arv muutujaid
if [ $# -eq 1 ]; then
   KODU=$1

   else
      echo "Kasuta skripti $(basename $0) VEEBIAADRESS"
      exit 1
fi   
#Kontrollib, kas selline veebikodu on juba olemas
grep $KODU /etc/hosts > /dev/null 2>&1 && echo "Selline leht eksisteerib" && exit 1

#Kontrollib, kas Apache on paigaldatud (vajadusel paigaldab)
type apache2 > /dev/null 2>&1

if [ $? -ne 0 ]
then
   apt-get update && apt-get install apache2 -y || exit 1
fi

#Nimelahenduse loomine
echo "127.0.0.1  $KODU" >> /etc/hosts

#Konfiguratsiooni faili kopeerimine /etc/apache2/sites-available/default.
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$KODU.conf

#Konfiguratsiooni faili muutmine
sed -i "s-#ServerName www.example.com-ServerName $KODU-g" /etc/apache2/sites-available/$KODU.conf
sed -i "s-/var/www/html-/var/www/$KODU-g" /etc/apache2/sites-available/$KODU.conf

#Veebisaidi füüsiline kodu
echo "Loon veebisaidi kataloogi: /var/www/$KODU"
mkdir -p /var/www/$KODU

#Default index faili kopeerimine
echo "Lisan vaikimisi index faili."
cp /var/www/html/index.html /var/www/$KODU/index.html

#Index faili muutmine
echo "Muudan index.html-i sisu"
echo "<html><head><title>$KODU</title></head><body>
<p>
[Intro]
Whole lotta lovin', lovin', lovin', lovin'
Whole lotta, whole lotta
Mustard on the beat, ho!

[Pre-Hook]
Aye mama, don't need no shit
I don't need no lip, just a whole lotta lovin' (yeah)
Niggas always tryna hate
But when I'm with they bitch, get a whole lotta lovin' (straight up!)

[Hook]
Finally I can move how I want
And I need, pick a time and I'm zoning
You got me, I got you and that's all that I need
When I wake in the morning
For a whole lotta lovin'
For a whole lotta lovin'
Whole lotta lovin'
Whole lotta lovin', lovin', lovin', lovin'
Whole lotta lovin'
Whole lotta lovin', lovin', lovin', lovin'
Whole lotta lovin'
Whole lotta lovin', lovin', lovin', lovin'
God damn it I'm fucked up
Are you down? Are you down? Are you down?
(God damn it I'm fucked up)
Are you down? Are you down? Are you down?
(God damn it I'm fucked up)

[Verse 1]
Wait, baby it's 'bout to go down (yeah!)
Order another round (yeah!)
Don't get nervous now
Turn them one's around (it's lit!)
Shining up that whip
Bad bitch on my dick (straight up!)
Time ain't going nowhere
Long as you right here (woo!)
Pour my lean and juice
Braid my hair like Snoop (yeah!)
Glidin' in my coupe
Ain't no bitch like you
Riders with me too
Die and live by you
Die and live by you
Yeah, yeah, yeah, yeah, yeah, yeah

[Hook]
Finally I can move how I want
And I need, pick a time and I'm zoning
You got me, I got you and that's all that I need
When I wake in the morning
For a whole lotta lovin'
For a whole lotta lovin'
Whole lotta lovin'
Whole lotta lovin', lovin', lovin', lovin'
Whole lotta lovin'
Whole lotta lovin', lovin', lovin', lovin'
Whole lotta lovin'
Whole lotta lovin', lovin', lovin', lovin'
God damn it I'm fucked up
Are you down? Are you down? Are you down?
(God damn it I'm fucked up)
Are you down? Are you down? Are you down?
(God damn it I'm fucked up)

[Verse 2]
Now it's time to take it home
Do at least a hundred, baby
Driving in your new Mercedes
That your daddy loaned us
Now it's time to take it home
Do at least a hundred, baby
Driving in your new Mercedes
That your daddy loaned us

[Outro]
Are you down? are you down? are you down?
Finally I can move how I wanted
Are you down? are you down? are you down?
You got me, I got you and that's all that I need
Are you down? are you down? are you down?
When I wake in the morning
Are you down? are you down? are you down?
</p></body></html>" > /var/www/$KODU/index.html

#Virtuaalserveri lubamine
a2ensite $KODU > /dev/null 2>&1

#Veebiserveri uuesti laadimine
/etc/init.d/apache2 reload > /dev/null 2>&1
exit 0