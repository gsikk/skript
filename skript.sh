#!/bin/bash
#Autor Gert Sikk
#Antud skript jagab loodud etteantud grupile uue kausta
#Kontrollin kas scripti jooksutatakse root õigustes.
if [ $EUID -ne 0 ]
then
    echo "Skripti palun jooksutada juur kasutajas!" 
    exit 1;
fi

#Kontrollin argumente
if [ $# -eq 2 ]; then
    KAUST=$1
    GROUP=$2
    SHARE=$(basename $KAUST)
    else
        if [ $# -eq 3 ]; then
            KAUST=$1
            GROUP=$2
            SHARE=$3
            else
                echo "Kasutada $(basename $0) KAUST GRUPP [SHARE]"
                exit 1;
            fi
fi

echo ${KAUST:0:1}

if [ ${KAUST:0:1} == "/" ];  then
	 KAUST=$KAUST
	echo "algab kaldkriipsuga"
else
	 KAUST=$(pwd)/$KAUST
	echo "ei alga kaldkriipsuga"
fi


#Kontrollin kas samba on installeeritud, vajadusel installeerin.
type smbd > /dev/null 2>&1

if [ $? -ne 0 ]; then
    apt-get update && apt-get install samba -y
else
    echo 'Samba on juba installitud!'
fi

#Testin, kas kaust test(Directory) käsuga, vajadusel loon kausta.
test -d $KAUST || mkdir -p $KAUST


#Testin, kas grupp on olemas, vajadusel loon selle.
getent group $GROUP > /dev/null || addgroup $GROUP > /dev/null

#Kasutan cp (copy käsku), et samba confi failist backup luua
cp /etc/samba/smb.conf /etc/samba/smb.conf.test

#Testi lisatakse uus confi rida
cat >> /etc/samba/smb.conf.test << EOF

[$SHARE]
    comment=share folder
    path=$KAUST
    writable=yes
    valid users=@$GROUP
    force group=$GROUP
    browsable=yes
    create mask=0664
    directory mask=0775
EOF

#Kontrollin testi confi(Kas ridade lisamine õnnestus)
testparm -s /etc/samba/smb.conf.test
if [ $? -ne 0 ]; then
    echo "Skriptis on viga!"
    exit 1;
fi

#Copyn testi confi algse asemele
cp /etc/samba/smb.conf.test /etc/samba/smb.conf

#Reloadin samba, et confi muudatusi arvestataks
/etc/init.d/smbd reload

#Annan kasutajale teada et kõik õnnestus.
echo "õnnestus!"
